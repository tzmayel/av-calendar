<?php
/**
 * Created by PhpStorm.
 * User: rent
 * Date: 23.7.2018 г.
 * Time: 06:12 ч.
 */

namespace Calendar;

use DateTimeInterface;
include 'CalendarInterface.php';


class Calendar implements CalendarInterface {
    var $theDate;

    public function __construct(DateTimeInterface $dt) {
        $this->theDate = $dt;
    }

    /**
     * Get the day
     *
     * @return int
     */
    public function getDay() {
        return (int) $this->theDate->format('d');
    }

    /**
     * Get the weekday (1-7, 1 = Monday)
     *
     * @return int
     */
    public function getWeekDay() {
        return (int) $this->theDate->format('N');
    }

    /**
     * Get the first weekday of this month (1-7, 1 = Monday)
     *
     * @return int
     */
    public function getFirstWeekDay() {
        $date = new \DateTime($this->theDate->format('Y-m-01'));
        return (int) $date->format('N');
    }

    /**
     * Get the first week of this month
     *
     * @return int
     */
    public function getFirstWeek() {
        $date = new \DateTime($this->theDate->format('Y-m-01'));
        return (int) $date->format('W');
    }

    /**
     * Get the number of days in this month
     *
     * @return int
     */
    public function getNumberOfDaysInThisMonth() {
        return (int) $this->theDate->format('t');
    }

    /**
     * Get the number of days in the previous month
     *
     * @return int
     */
    public function getNumberOfDaysInPreviousMonth() {
        $date = new \DateTime($this->theDate->format('Y-m-d'));
        $date ->modify('last day of previous month');
        return (int) $date->format('d');
    }

    /**
     * Get the calendar array
     *
     * @return array
     */
    public function getCalendar() {
        $result = [];
        $startWeekNumber = $this->getFirstWeek();
        $year = (int)$this->theDate->format('Y');

        //find first Monday
        $date = new \DateTime();

        if($startWeekNumber == 53) {
            $year--;
        }
        $date->setISODate($year, $startWeekNumber);

        for($week = 0 ; $week<6; $week++){
            $currentWeek = (int)$date->format('W');
            $isHighlighted = false;

            $prevWeekNumber = ($this->theDate->format('W') - 1);
            if($prevWeekNumber == 0){
                $prevWeekNumber = 53;
            }

            if($currentWeek === $prevWeekNumber){
                $isHighlighted = true;
            }

            for($day = 0; $day < 7; $day ++){
                $monthDate = (int)$date->format('d');
                $result[$currentWeek][$monthDate] = $isHighlighted;
                $date->modify('+1 day');
            }

            if($date->format('n') != $this->theDate->format('n')){
                break;
            }
        }
        return $result;
    }
}